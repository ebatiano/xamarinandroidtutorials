﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;

namespace Notifications
{
    public partial class NotificationSamples
    {
        // id for summary notification.
        private const int GroupSummaryId = int.MaxValue;

        readonly Dictionary<int, Notification> _groupedNotifications = new Dictionary<int, Notification>();

        private int _requestCodeForIntent = 1;
        private int _groupedNotificationsCount = 0;

        readonly Context _context;

        private NotificationManager NotificationManager => (NotificationManager)_context.GetSystemService(Context.NotificationService);
        private int NextNotificationId { get; set; } = 1;

		public NotificationSamples(Context context)
		{
		    _context = context;
		}

		/// <summary>
        /// Dismisses notification with given id.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        public void DismissNotification(int notificationId)
        {
            NotificationManager.Cancel(notificationId);

            // Remove from grouped Dictionary, so count is valid.
            _groupedNotifications.Remove(notificationId);

            // If group summary is removed, clear all group notifications and reset grouped counter.
            if (notificationId == GroupSummaryId)
            {
                _groupedNotificationsCount = 0;

                foreach (var key in _groupedNotifications)
                {
                    NotificationManager.Cancel(key.Key);
                }
                _groupedNotifications.Clear();
            }

            // Update info for group summary.
            UpdateGroupInfo();
        }

        /// <summary>
        /// Creates notification core. Every notification needs icon, title and escription.
        /// </summary>
        /// <param name="title">Title of notification.</param>
        /// <param name="desc">Description of notification.</param>
        /// <returns></returns>
        private Notification.Builder CreateNotificationCore(string title, string desc)
        {
            // 'this' is of type Context.
            var builder = new Notification.Builder(_context);

            // Set notification required parameters.
            builder
                .SetSmallIcon(Resource.Drawable.Icon)
                .SetContentTitle(title)
                .SetContentText(desc);

            return builder;
        }

        /// <summary>
        /// Creates PendingIntent for action, wchich opens main activity with flags.
        /// </summary>
        /// <param name="actionName"></param>
        /// <returns></returns>
        private PendingIntent GetPendingIntentForAction(string actionName)
        {
            // Creates an intent for an Activity in your app
            var intent = new Intent(_context, typeof(MainActivity));

            // Put action and notification id to extras.
            intent.PutExtra("action", actionName);
            intent.PutExtra("notificationId", NextNotificationId);

            // Set intent to show activity on top.
            intent.SetFlags(ActivityFlags.ReorderToFront);

            // Create pending intent which starts or brings to fron main activity.
            var pendingIntent = PendingIntent.GetActivity(_context, ++_requestCodeForIntent, intent, PendingIntentFlags.OneShot);

            return pendingIntent;
        }

        /// <summary>
        /// Updates info on summary notification.
        /// </summary>
        private void UpdateGroupInfo()
        {
            if ((int)Build.VERSION.SdkInt < 24)
            {
                return;
            }

            var notificationsCount = _groupedNotifications.Count;

            // Add group summary if more than one notification.
            if (notificationsCount > 0)
            {
                // 'this' is of type Context.
                var summaryBuilder = new Notification.Builder(_context);

                // Set notification required parameters.
                summaryBuilder.SetSmallIcon(Resource.Drawable.Icon);

                // Set group id for notification.
                summaryBuilder.SetGroup("sampleGroup");

                // Enable notification as group summary.
                summaryBuilder.SetGroupSummary(true);

                // Set summary style with proper description (It should be short).
                summaryBuilder.SetStyle(
                    new Notification.InboxStyle()
                        .SetSummaryText($"{notificationsCount} notifications")
                        );

                // Update/show summary notification.
                NotificationManager.Notify(GroupSummaryId, summaryBuilder.Build());
            }
            else
            {
                // Remove summary if no grouped notifications.
                NotificationManager.Cancel(GroupSummaryId);
            }
        }
    }
}