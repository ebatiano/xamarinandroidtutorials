﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Content.PM;

namespace Notifications
{
    [Activity(
        Label = "Notifications",
        MainLauncher = true,
        LaunchMode = LaunchMode.SingleTask,
        AlwaysRetainTaskState = true,
        Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private NotificationSamples _samples;

        /// <summary>
        /// Initializes activity and attaches buttons' actions.
        /// </summary>
        /// <param name="bundle">Bundle.</param>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _samples = new NotificationSamples(this);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            AddAction(Resource.Id.ButtonBasicNotification, _samples.ShowBasicNotification);
            AddAction(Resource.Id.ButtonDefaultIntent, _samples.ShowDefaultIntent);
            AddAction(Resource.Id.ButtonWithActions, _samples.ShowWithActions);
            AddAction(Resource.Id.ButtonGroup, _samples.ShowGroup);
            AddAction(Resource.Id.ButtonProgressBar, _samples.ShowProgressBar);
            AddAction(Resource.Id.ButtonActivityIndicator, _samples.ShowActivityIndicator);
            AddAction(Resource.Id.ButtonLockScreenNotifications, _samples.ShowLockScreenNotifications);
            AddAction(Resource.Id.ButtonCustomViewNotification, _samples.ShowCustomViewNotification);
            AddAction(Resource.Id.ButtonCustomViewNotificationBig, _samples.ShowCustomViewNotificationBig);
        }

        /// <summary>
        /// Handles new intent event.
        /// </summary>
        /// <param name="intent">Intent passed to activity.</param>
        protected override void OnNewIntent(Intent intent)
        {
            // Get action string and process it.
            switch (intent.GetStringExtra("action"))
            {
                case "yes":
                    Toast.MakeText(this, "Yes clicked", ToastLength.Short).Show();
                    break;

                case "no":
                    Toast.MakeText(this, "No clicked", ToastLength.Short).Show();
                    break;

                case "delete":
                    break;

                default:
                    Toast.MakeText(this, "Default notification action", ToastLength.Short).Show();
                    break;
            }

            // Get notification id and remove it from notifications area.
            var notificationId = intent.GetIntExtra("notificationId", 0);

            _samples.DismissNotification(notificationId);
        }

        /// <summary>
        /// Attaches action to button.
        /// </summary>
        /// <param name="buttonId">Id of button.</param>
        /// <param name="action">Action to perform when clicked.</param>
        private void AddAction(int buttonId, Action action)
        {
            var button = FindViewById<Button>(buttonId);
            button.Click += (e, a) => action();
        }
    }
}

