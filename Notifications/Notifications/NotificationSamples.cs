using System.Threading.Tasks;
using Android.App;
using Android.Media;
using Android.OS;
using Android.Renderscripts;
using Android.Widget;

namespace Notifications
{
    public partial class NotificationSamples
    {
        /// <summary>
        /// Shows basic notification.
        /// </summary>
        public void ShowBasicNotification()
        {
            var builder = CreateNotificationCore("Sample Notification", "This is a sample notification.");

            // Always increase notification id after notifying manager.
            NotificationManager.Notify(NextNotificationId++, builder.Build());
        }

        /// <summary>
        /// Shows notification with attached default intent.
        /// </summary>
        public void ShowDefaultIntent()
        {
            var builder = CreateNotificationCore("Default Intent Sample", "Click notification to start something.");

            var pendingIntent = GetPendingIntentForAction("default");
            builder.SetContentIntent(pendingIntent);

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());
        }

        /// <summary>
        /// Shows notifications with two actions.
        /// </summary>
        public void ShowWithActions()
        {
            var builder = CreateNotificationCore("Actions Sample", "Click action to do something.");

            // Generate intents for yes and no.
            var yesIntent = GetPendingIntentForAction("yes");
            var noIntent = GetPendingIntentForAction("no");

            // Create the yes and no actions.
            var actionYes = new Notification.Action.Builder(Resource.Drawable.Yes, "Yes", yesIntent).Build();
            var actionNo = new Notification.Action.Builder(Resource.Drawable.No, "No", noIntent).Build();

            // Add yes and no actions to builder.
            builder.AddAction(actionYes).AddAction(actionNo);

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());
        }

        /// <summary>
        /// Creates notification which goes to group of notifications.
        /// </summary>
        public void ShowGroup()
        {
            if ((int)Build.VERSION.SdkInt >= 24)
            {
                var info = $"This is notification with id {NextNotificationId + 1}.";

                var builder = CreateNotificationCore("Group Sample", info);

                builder.SetGroup("sampleGroup");
                builder.SetDeleteIntent(GetPendingIntentForAction("delete"));

                var notification = builder.Build();

                // Add notification to map.
                _groupedNotifications.Add(NextNotificationId, notification);

                // Show notification.
                NotificationManager.Notify(NextNotificationId++, notification);

                UpdateGroupInfo();
            }
            else // For Android older than 7.0
            {
                _groupedNotificationsCount++;
                var builder = CreateNotificationCore("Grouped notifications",
                        $"You've created {_groupedNotificationsCount} notifications in group.");

                // Update/show summary notification.
                NotificationManager.Notify(GroupSummaryId, builder.Build());
            }
        }

        /// <summary>
        /// Shows notification with progress bar.
        /// </summary>
        public void ShowProgressBar()
        {
            var id = NextNotificationId++;

            // Start something in background.
            Task.Factory.StartNew(async () =>
            {
                var builder = CreateNotificationCore("ProgressBar Sample", "Fancy progress...");

                var progress = 0;

                while (progress <= 100)
                {
                    builder
                        .SetProgress(100, progress, false)
                        .SetOngoing(true);

                    // Show notification.
                    NotificationManager.Notify(id, builder.Build());

                    progress++;
                    await Task.Delay(200);
                }

                // Show notification.
                NotificationManager.Cancel(id);
            });
        }

        /// <summary>
        /// Shows notification with activity indicator.
        /// </summary>
        public void ShowActivityIndicator()
        {
            var id = NextNotificationId++;

            // Start something in background.
            Task.Factory.StartNew(async () =>
            {
                var builder = CreateNotificationCore("Activity indicator sample", "Unknown duration operation...");

                builder
                    .SetProgress(0, 0, true)
                    .SetOngoing(true);

                // Show notification.
                NotificationManager.Notify(id, builder.Build());

                // Wait for some time.
                await Task.Delay(10000);

                // Show notification.
                NotificationManager.Cancel(id);
            });
        }

        /// <summary>
        /// SHows two notification on lock screen - one private and one public and one secret, which is not visible on lock screen.
        /// </summary>
        public void ShowLockScreenNotifications()
        {
            var builder = CreateNotificationCore("Public notification", "This is a public notification, you can read its content on lock screen.");
            builder.SetVisibility(NotificationVisibility.Public);

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());

            builder = CreateNotificationCore("Private notification", "This is a private notification, you can read its content only on unlocked screen.");
            builder.SetVisibility(NotificationVisibility.Private);

            // Set sound for notification.
            builder.SetSound(RingtoneManager.GetActualDefaultRingtoneUri(_context, RingtoneType.Notification));

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());

            builder = CreateNotificationCore("Secret notification", "This is a secret notification. You won't see it on locked screen.");
            builder.SetVisibility(NotificationVisibility.Secret);

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());
        }

        public void ShowCustomViewNotification()
        {
            // Create remote view from layout.
            RemoteViews remoteViews = new RemoteViews(_context.PackageName, Resource.Layout.CustomNotification);

            // Set layout data - image, title and text.
            remoteViews.SetImageViewResource(Resource.Id.CustomImage, Resource.Drawable.Icon);
            remoteViews.SetTextViewText(Resource.Id.CustomTitle, "Custom Layout");
            remoteViews.SetTextViewText(Resource.Id.CustomText, "This is a custom layout notification.");

            var builder = new Notification.Builder(_context);

            builder.SetSmallIcon(Resource.Drawable.Icon) // Only small icon is necessary for custom layout.
                .SetCustomContentView(remoteViews);   // Set content view using either SetCustomContentView or SetCustomBigContentView.

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());
        }

        public void ShowCustomViewNotificationBig()
        {
            // Create remote view from layout.
            RemoteViews remoteViews = new RemoteViews(_context.PackageName, Resource.Layout.CustomNotificationBig);

            // Set layout data - image, title and text.
            remoteViews.SetImageViewResource(Resource.Id.CustomImage, Resource.Drawable.Icon);
            remoteViews.SetTextViewText(Resource.Id.CustomTitle, "Custom Layout");
            remoteViews.SetTextViewText(Resource.Id.CustomText, "This is a custom layout notification.");

            var builder = new Notification.Builder(_context);

            builder.SetSmallIcon(Resource.Drawable.Icon) // Only small icon is necessary for custom layout.
                .SetCustomBigContentView(remoteViews);   // Set content view using either SetCustomContentView or SetCustomBigContentView.

            // Show notification.
            NotificationManager.Notify(NextNotificationId++, builder.Build());
        }
    }
}